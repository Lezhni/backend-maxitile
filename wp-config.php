<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'maxitile_dev');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ' h?! %_DpnVX#E3]}?{ehM+WMk)QK3/#Bc+<>|]&O;p8%uqn(GrV)4|t=Z.<|04m');
define('SECURE_AUTH_KEY',  'Fv?B@{IQ|*Lz)!VFVPKTp]?W$o[`m)).8F&ol*Lg*n>CF,SZTx>NZ.61fZ2n-pq|');
define('LOGGED_IN_KEY',    '34m|,|;=&d{`E[V:C`crVH{|=7%)1oi-~o_OhCbqHK[2vf6Bxuqs-Xe~2HY{b3I}');
define('NONCE_KEY',        'qg llN6PU=Ez`{G&N%zUqFl1<%Gx#sSkS0O&({{uWF3K?h>zQO%ZAhXWJ*G6&ee9');
define('AUTH_SALT',        'iyKE(^#gS:AE2b$HN/ 5?/tsvJ:-|L;g[DCCbYbcB`KD2&W_,V>-N l(+#@.Q/{>');
define('SECURE_AUTH_SALT', 'u|l6PpDO0_#p@^O|zi-dmk&k%yj dgGLA9j0d%grKo4e/n)xxnEWAt33qYLRqBfw');
define('LOGGED_IN_SALT',   '{%}lRMqlu* |2F.i^%8%_@[~S[`DmXcnIG:Htx S!IBUv9bt$x<]IsJnjb`Z9]*d');
define('NONCE_SALT',       '$++53-~d}D}d1BGI]sN4oao,oc`tafe#VG$$+93FNhRMrii@M`O]4Ufa|g{I8j>$');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'mt_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('WP_MEMORY_LIMIT', '128M');

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
