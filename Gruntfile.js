module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        less: {
            production: {
                files: {
                    "wp-content/themes/maxitile/css/style.css": "wp-content/themes/maxitile/less/style.less"
                }
            }
        },
        autoprefixer: {
            options: {
                browsers: ['last 10 versions', 'Firefox > 15', 'Opera >= 12']
            },
            build: {
                src: 'wp-content/themes/maxitile/css/style.css',
                dest: 'wp-content/themes/maxitile/css/style.css'
            }
        },
        concat_css: {
            all: {
              src: ["wp-content/themes/maxitile/css/*.css"],
              dest: "wp-content/themes/maxitile/css/prod/style.css"
            }
        },
        cssmin: {
            target: {
                files: {
                    'wp-content/themes/maxitile/css/prod/style.min.css': 'wp-content/themes/maxitile/css/prod/style.css'
                }
            }
        },
        concat: {
            dist: {
                src: [
                    'wp-content/themes/maxitile/js/*.js'
                ],
                dest: 'wp-content/themes/maxitile/js/prod/main.js',
            }
        },
        uglify: {
            build: {
                src: 'wp-content/themes/maxitile/js/prod/main.js',
                dest: 'wp-content/themes/maxitile/js/prod/main.min.js'
            }
        },
        imagemin : {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'wp-content/themes/maxitile/img/raw/',
                    src: ['**/*.{png,jpg,gif,svg}'],
                    dest: 'wp-content/themes/maxitile/img/'
                }]
            }
        },
        watch: {
            styles: {
                files: ['wp-content/themes/maxitile/less/*.less'],
                tasks: ['less', 'autoprefixer','concat_css', 'cssmin'],
                options: {
                    nospawn: true
                }
            },
            scripts: {
                files: ['wp-content/themes/maxitile/js/*.js'],
                tasks: ['concat', 'uglify'],
                options: {
                    nospawn: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('images', ['imagemin:dynamic']);
};