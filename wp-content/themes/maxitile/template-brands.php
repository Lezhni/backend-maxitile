<?php
/**
 * Template Name: Производители
 */
?>

<?php get_header(); ?>

<?php if (have_posts()) : ?>
	<main class="gl grey-bg content-wrapper" role="main">
		<div class="container">
			<nav class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
			    <?php if(function_exists('bcn_display')) : ?>
			        <?php bcn_display(); ?>
			    <?php endif; ?>
			</nav>

			<?php get_sidebar(); ?>

			<section class="content to-right">
				<div class="content-page catalog-page">
					<?php while (have_posts()) : the_post(); ?>
						<h1 class="page-title"><?php the_title(); ?></h1>
						<?php the_content(); ?>
					<?php endwhile; ?>
				</div>
				<?php
					// Get all 'brand' attribute's values
					$brands = array();
					$query = array(
					    'nopaging' => true,
					    'no_found_rows' => 1,
					    'post_status' => 'publish',
					    'post_type' => 'product',
					    'orderby' =>'name',
					    'order' => 'ASC',
					    'meta_query' => WC()->query->get_meta_query()
					);
					$all_products = new WP_Query($query);
					if ($all_products->have_posts()) {
						while ($all_products->have_posts()) {
							$all_products->the_post();
							if (($brand = $product->get_attribute('brand')) != '')
								$brands[] = $brand;
						}
					}
					$brands = array_unique($brands);

					wp_reset_query();

				?>
				<?php if (count($brands) > 0) : ?>
					<div class="goods-list catalog-goods-list brands-list">
						<?php foreach ($brands as $brand) : ?>

							<?php
								$brand = strtolower(str_replace(' ', '-', $brand));
								$attr = array('tag' => $brand);
								$page = new WP_Query($attr);
							?>
							<?php if ($page->have_posts()) : $page->the_post(); ?>
								<div class="goods-single brand-single">
									<div class="brand-single-image"><?php echo get_the_post_thumbnail(); ?></div>
									<h3 class="brand-single-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<div class="brand-single-desc"><?php the_excerpt(); ?></div>
								</div>
							<?php endif; ?>

							<?php wp_reset_query(); ?>

						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</section>
		</div>
	</main>
<?php endif; ?>

<?php get_footer(); ?>
