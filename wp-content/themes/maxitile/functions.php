<?php

// Woocoommerce
add_theme_support( 'woocommerce' );

add_filter('loop_shop_per_page', create_function('$cols', 'return 12;'), 20);

remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {

  echo '<main class="gl grey-bg content-wrapper" role="main"><div class="container">';
}
function my_theme_wrapper_end() {

  echo '</div></main>';
}

remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);

add_action('woocommerce_before_single_product_summary', 'on_sale_label', 10);

function on_sale_label() {

    return false;
}

add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

// Numeric nouns
function declOfNum($number, $titles) {

	$cases = array (2, 0, 1, 1, 1, 2);
	return $number." ".$titles[ ($number%100 > 4 && $number %100 < 20) ? 2 : $cases[min($number%10, 5)] ];
}

// Connect custom css
if (!is_admin()) {
    wp_register_style(
        'maxitile-styles',
        get_bloginfo('stylesheet_directory') . '/css/prod/style.min.css',
        false,
        0.1
    );
    wp_enqueue_style('maxitile-styles');
}

// Connect menus
function maxitile_menus() {

    register_nav_menu('header-menu', __('Верхнее меню'));
    register_nav_menu('footer-menu-1', __('Нижнее меню 1'));
    register_nav_menu('footer-menu-2', __('Нижнее меню 2'));
    register_nav_menu('footer-menu-3', __('Нижнее меню 3'));
}

// Add tag support to pages
function tags_support_all() {

    register_taxonomy_for_object_type('post_tag', 'page');
}

// Ensure all tags are included in queries
function tags_support_query($wp_query) {

    if ($wp_query->get('tag')) $wp_query->set('post_type', 'any');
}

// Enable AJAX cart update
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	ob_start();
	?>
  <div class="cart to-left">
    <span class="cart-name">Корзина</span>
    <?php global $woocommerce; ?>
    <?php if (sizeof($woocommerce->cart->cart_contents) > 0) : ?>
      <?php
        $num = $woocommerce->cart->cart_contents_count;
        $goods = declOfNum($num, array('товар', 'товара', 'товаров'));
      ?>
      <span class="cart-num"><?php echo $goods; ?></span>
    <?php else: ?>
      <span class="cart-num">пуста</span>
    <?php endif; ?>
  </div>
	<?php

	$fragments['div.cart'] = ob_get_clean();

	return $fragments;
}

// Enable PrettyPhoto for whole site
add_action( 'wp_enqueue_scripts', 'frontend_scripts_include_lightbox' );
function frontend_scripts_include_lightbox() {
  global $woocommerce;
  $suffix      = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
  $lightbox_en = get_option( 'woocommerce_enable_lightbox' ) == 'yes' ? true : false;

  if ( $lightbox_en ) {
    wp_enqueue_script( 'prettyPhoto', $woocommerce->plugin_url() . '/assets/js/prettyPhoto/jquery.prettyPhoto' . $suffix . '.js', array( 'jquery' ), $woocommerce->version, true );
    wp_enqueue_script( 'prettyPhoto-init', $woocommerce->plugin_url() . '/assets/js/prettyPhoto/jquery.prettyPhoto.init' . $suffix . '.js', array( 'jquery' ), $woocommerce->version, true );
    wp_enqueue_style( 'woocommerce_prettyPhoto_css', $woocommerce->plugin_url() . '/assets/css/prettyPhoto.css' );
  }
}

// Tag hooks
add_action('init', 'tags_support_all');
add_action('pre_get_posts', 'tags_support_query');

// Add sidebar
function sidebar_widgets_init() {

    register_sidebar( array(
        'name'          => 'Боковая панель',
        'id'            => 'right_sidebar',
        'description' => 'Основная боковая панель для размещения виджетов (фильтров, меню, пр)',
        'before_widget' => '<div id="%1$s" class="sidebar-block to-left %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ) );

}

// Add fields to settings
function site_custom_settings() {

    add_settings_section(
        'site_custom_settings',
        'Настройки сайта',
        'site_custom_settings_callback',
        'general'
    );

    add_settings_field(
        'contact_phone',
        'Контактный телефон',
        'my_textbox_callback',
        'general',
        'site_custom_settings',
        array(
            'contact_phone'
        )
    );
    add_settings_field(
        'contact_email',
        'Контактный email',
        'my_textbox_callback',
        'general',
        'site_custom_settings',
        array(
            'contact_email'
        )
    );
    add_settings_field(
        'contact_address',
        'Адрес магазина',
        'my_textbox_callback',
        'general',
        'site_custom_settings',
        array(
            'contact_address'
        )
    );
    add_settings_field(
        'contact_copyrights',
        'Информация в подвале',
        'my_textarea_callback',
        'general',
        'site_custom_settings',
        array(
            'contact_copyrights'
        )
    );

    register_setting('general','contact_email', 'esc_attr');
    register_setting('general','contact_phone', 'esc_attr');
    register_setting('general','contact_address', 'esc_attr');
    register_setting('general','contact_copyrights', 'esc_attr');
}

function site_custom_settings_callback() {
}

function my_textbox_callback($args) {

    $option = get_option($args[0]);
    echo '<input type="text" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" />';
}
function my_textarea_callback($args) {

    $option = get_option($args[0]);
    echo '<textarea id="'. $args[0] .'" cols="70" rows="10" name="'. $args[0] .'">' . $option . '</textarea>';
}

add_action('init', 'maxitile_menus');
add_action('admin_init', 'site_custom_settings');
add_action('widgets_init', 'sidebar_widgets_init');
