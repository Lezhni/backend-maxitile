<?php get_header(); ?>

<?php

global $query_string;
$query_args = explode("&", $query_string);
$search_query = array();

foreach($query_args as $key => $string) {
  $query_split = explode("=", $string);
  $search_query[$query_split[0]] = urldecode($query_split[1]);
}

$the_query = new WP_Query($search_query);

?>

<main class="gl grey-bg content-wrapper" role="main">
	<div class="container">
		<nav class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
		    <?php if(function_exists('bcn_display')) : ?>
		        <?php bcn_display(); ?>
		    <?php endif; ?>
		</nav>

    <?php get_sidebar(); ?>

		<section class="content to-right">
			<div class="content-page text-page search-page">
				<?php if ($the_query->have_posts()) : ?>
					<h1 class="page-title">Результаты поиска</h1>
					<h3>По Вашему запросу найдено <?php echo $the_query->found_posts; ?> результа(ов)</h3>
					<ul class="search-result-list">
					<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

						<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

					<?php endwhile; ?>
					</ul>
				<?php else: ?>
					<h1 class="page-title">Поиск не дал результатов</h1>
					<p>Попробуйте изменить поисковой запрос, или вернитесь на <a href="<?php bloginfo('home'); ?>">главную</a></p>
				<?php endif; ?>
			</div>
		</section>
    <div class="clearfix"></div>
	</div>
</main>

<?php wp_reset_postdata(); ?>

<?php get_footer(); ?>
