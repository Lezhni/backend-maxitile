<?php
/**
 * Template Name: Страница фабрики
 */
?>

<?php get_header(); ?>

<?php if (have_posts()) : ?>
	<main class="gl grey-bg content-wrapper" role="main">
		<div class="container">
			<nav class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
			    <?php if(function_exists('bcn_display')) : ?>
			        <?php bcn_display(); ?>
			    <?php endif; ?>
			</nav>

			<?php get_sidebar(); ?>

			<section class="content to-right">
				<div class="content-page catalog-page">
					<?php while (have_posts()) : the_post(); ?>
						<h1 class="page-title"><?php the_title(); ?></h1>
						<?php the_content(); ?>
						<?php $brand_tag = get_the_tags()[0]->slug; ?>
					<?php endwhile; ?>
				</div>
				<?php
					$collections = array();
					$query = array(
					    'nopaging' => true,
					    'post_type' => 'product',
					    'orderby' =>'name',
					    'order' => 'ASC',
					    'meta_query' => WC()->query->get_meta_query(),
					    'tax_query' => array(
					    	array(
					    		'taxonomy' => 'pa_brand',
								'terms' => $brand_tag,
								'field' => 'slug',
								'operator' => 'IN'
							)
						)
					);
					$all_products = new WP_Query($query);
					if ($all_products->have_posts()) {
						while ($all_products->have_posts()) {
							$all_products->the_post();
							$taxonomy = 'product_cat';
							$collections[] = get_the_terms($product->ID, $taxonomy);
						}
					}
					//$collections = array_unique($collections);
					foreach($collections as $element) {
				    $hash = $element[0]->name;
				    $unique_array[$hash] = $element;
					}
					$collections = $unique_array;
					wp_reset_query();

				?>
				<?php if (count($collections) > 0) : ?>
					<div class="goods-list catalog-goods-list collections-list">
						<?php foreach ($collections as $collection) : ?>
							<?php $collection = $collection[0]; ?>
							<div class="goods-single collection-single">
							<?php
								$the_permalink = get_term_link($collection->term_id, 'product_cat');
								$the_title = $collection->name;
							?>
								<a href="<?php echo $the_permalink; ?>" class="goods-single-img">
									<?php woocommerce_subcategory_thumbnail($collection) ?>
								</a>
								<div class="goods-single-content">
									<h3 class="goods-single-title"><a href="<?php echo $the_permalink; ?>"><?php echo $the_title; ?></a></h3>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</section>
			<div class="clearfix"></div>
		</div>
	</main>
<?php endif; ?>

<?php get_footer(); ?>
