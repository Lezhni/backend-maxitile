	<footer class="gl">
		<div class="container">
			<address class="footer-contacts to-left">
				© Maxitile.ru, 2015<br>
				ЗАО «МаксиТайл»<br>
				пр-т Независимости, 168/2, пом. 11Н, Москва, 220141, Российская Федерация
			</address>
			<div class="footer-block to-left">
				<?php
					$menu_location = 'footer-menu-1';
					$menu_locations = get_nav_menu_locations();
					$menu_object = (isset($menu_locations[$menu_location]) ? wp_get_nav_menu_object($menu_locations[$menu_location]) : null);
					$menu_name = (isset($menu_object->name) ? $menu_object->name : '');
					$fm1title = esc_html($menu_name);
				?>
				<strong class="footer-block-title"><?php echo $fm1title; ?></strong>
				<nav>
					<?php wp_nav_menu(array(
						'theme_location' => 'footer-menu-1'
						));
					?>
				</nav>
			</div>
			<div class="footer-block to-left">
				<?php
					$menu_location = 'footer-menu-2';
					$menu_locations = get_nav_menu_locations();
					$menu_object = (isset($menu_locations[$menu_location]) ? wp_get_nav_menu_object($menu_locations[$menu_location]) : null);
					$menu_name = (isset($menu_object->name) ? $menu_object->name : '');
					$fm2title = esc_html($menu_name);
				?>
				<strong class="footer-block-title"><?php echo $fm2title; ?></strong>
				<nav>
					<?php wp_nav_menu(array(
						'theme_location' => 'footer-menu-2'
						));
					?>
				</nav>
			</div>
			<div class="footer-block to-left">
				<?php
					$menu_location = 'footer-menu-3';
					$menu_locations = get_nav_menu_locations();
					$menu_object = (isset($menu_locations[$menu_location]) ? wp_get_nav_menu_object($menu_locations[$menu_location]) : null);
					$menu_name = (isset($menu_object->name) ? $menu_object->name : '');
					$fm3title = esc_html($menu_name);
				?>
				<strong class="footer-block-title"><?php echo $fm3title; ?></strong>
				<nav>
					<?php wp_nav_menu(array(
						'theme_location' => 'footer-menu-3'
						));
					?>
				</nav>
			</div>
			<div class="clearfix"></div>
		</div>
	</footer>
	<aside class="add-to-cart-notice">
		<p></p>
	</aside>
	<?php wp_footer(); ?>
	<script src="<?php bloginfo('template_url'); ?>/js/prod/main.min.js"></script>
</body>
</html>
