<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title(); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=cyrillic' rel='stylesheet' type='text/css'>
<?php if (is_singular() && get_option('thread_comments')) wp_enqueue_script('comment-reply'); ?>
<?php wp_head(); ?>
</head>
<?php
	$widgets_list = wp_get_sidebars_widgets();
	$right_sidebar = $widgets_list['right_sidebar'];
	$is_sidebar = ( !in_array('woocommerce_product_categories-2', $right_sidebar) ) ? 'without-sidebar' : 'with-sidebar';
?>
<body <?php body_class( $is_sidebar ); ?>>
	<div class="modal mfp-hide" id="request">
		<p class="modal-title">Оставить заявку на <br>"<span class="goods-name"></span>"</p>
		<p>Как только товар появится в продаже,<br>Вы об этом тут же узнаете</p>
		<?php echo do_shortcode('[contact-form-7 id="88" title="Заявка на товар"]'); ?>
		<?php if (is_user_logged_in()) : ?>
			<?php
				$user =  wp_get_current_user();
				$phone = get_user_meta($user->ID, 'billing_phone')[0];
				$name = get_user_meta($user->ID, 'billing_first_name')[0] . ' ' . get_user_meta($user->ID, 'billing_last_name')[0];
			 	$email = get_user_meta($user->ID, 'billing_email')[0];
			?>
			<input type="hidden" class="request-name" value="<?php echo $name; ?>">
			<input type="hidden" class="request-email" value="<?php echo $email; ?>">
			<input type="hidden" class="request-phone" value="<?php echo $phone; ?>">
		<?php endif; ?>
	</div>
	<div class="top-panel grey-bg">
		<div class="container">
			<nav class="top-panel-menu">
				<?php wp_nav_menu(array(
					'theme_location' => 'header-menu'
					));
				?>
			</nav>
			<nav class="top-panel-user">
				<?php if ( is_user_logged_in() ) : ?>
					<?php $current_user = wp_get_current_user(); ?>
				 	<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"><?php echo 'Привет, ' . (($current_user->user_firstname != '') ? $current_user->user_firstname : $current_user->user_login) . '!'; ?></a>
				 	<a href="<?php echo wp_logout_url( get_permalink() ); ?>">Выход.</a>
				 <?php else : ?>
				 	<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">Вход / Регистрация</a>
				 <?php endif; ?>
			</nav>
		</div>
	</div>
	<header class="gl" role="banner">
		<div class="container">
			<a href="<?php bloginfo( 'url' ); ?>" class="logo to-left"></a>
			<div class="info-blocks to-left">
				<div class="header-info-block">
					<p class="info-block-title">Контактный телефон:</p>
					<address class="info-block-text"><a href="callto:<?php echo trim(get_option('contact_phone')); ?>"><img src="<?php bloginfo('template_url') ?>/img/icon-phone-red.png" alt=""><?php echo get_option('contact_phone'); ?></a></address>
				</div>
				<div class="header-info-block">
					<p class="info-block-title">Email для вопросов:</p>
					<address class="info-block-text"><a href="mailto:<?php echo get_option('contact_email'); ?>"><img src="<?php bloginfo('template_url') ?>/img/icon-email-red.png" alt=""><?php echo get_option('contact_email'); ?></a></address>
				</div>
				<div class="header-info-block">
					<p class="info-block-title">Адрес офиса:</p>
					<address class="info-block-text"><img src="<?php bloginfo('template_url') ?>/img/icon-map-red.png" alt=""><?php echo get_option('contact_address'); ?></address>
				</div>
			</div>
			<?php global $woocommerce; ?>
			<a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" class="cart-wrapper to-right">
				<span class="cart-img to-left"></span>
				<div class="cart to-left">
					<span class="cart-name">Корзина</span>
					<?php if (sizeof($woocommerce->cart->cart_contents) > 0) : ?>
						<?php
							$num = $woocommerce->cart->cart_contents_count;
							$goods = declOfNum($num, array('товар', 'товара', 'товаров'));
						?>
						<span class="cart-num"><?php echo $goods; ?></span>
					<?php else: ?>
						<span class="cart-num">пуста</span>
					<?php endif; ?>
				</div>
			</a>
			<div class="clearfix"></div>
			<div class="header-bottom-panel">
				<nav class="bottom-panel-menu to-left">
					<a href="<?php echo get_permalink(woocommerce_get_page_id('shop')); ?>" class="catalog-link">Каталог</a>
					<a href="/for-designers" class="designers-link">Дизайнерам</a>
				</nav>
				<div class="search-panel to-right">
					<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</header>
	<!-- <section class="advantages-block">
		<div class="container">
			<div class="advantages-list">
				<div class="advantage-single">
					<div class="img-wrapper">
						<img src="http://maxitile.dev/wp-content/themes/maxitile/img/icon-money-red.png" alt="">
					</div>
					<div class="advantage-single-content">
						<b>Лучшие цены</b>
						<p>в Интернете</p>
					</div>
				</div>
				<div class="advantage-single">
					<div class="img-wrapper">
						<img src="http://maxitile.dev/wp-content/themes/maxitile/img/icon-garrancy-red.png" alt="">
					</div>
					<div class="advantage-single-content">
						<b>Официальный дистрибьютор</b>
						<p>не занимаемся перекупом</p>
					</div>
				</div>
				<div class="advantage-single">
					<div class="img-wrapper">
						<img src="http://maxitile.dev/wp-content/themes/maxitile/img/icon-home-red.png" alt="">
					</div>
					<div class="advantage-single-content">
						<b>Большой склад в России</b>
						<p>вам не придется долго ждать!</p>
					</div>
				</div>
				<div class="advantage-single">
					<div class="img-wrapper">
						<img src="http://maxitile.dev/wp-content/themes/maxitile/img/icon-car-red.png" alt="">
					</div>
					<div class="advantage-single-content">
						<b>Поставки</b>
						<p>минимальные сроки поставок</p>
					</div>
				</div>
			</div>
		</div>
	</section> -->
	<?php if (!is_home()) : ?>
		<nav class="category-tabs">
			<div class="container">
				<ul>
					<li <?php if (is_archive() || get_post_type() == 'product') echo 'class="current"'; ?>><a href="<?php echo get_permalink(woocommerce_get_page_id('shop')); ?>">Коллекции</a></li>
					<li <?php if (strpos($_SERVER['REQUEST_URI'], 'brands')) echo 'class="current"'; ?>><a href="<?php bloginfo('home'); ?>/brands/">Производители</a></li>
				</ul>
			</div>
		</nav>
	<?php endif; ?>
