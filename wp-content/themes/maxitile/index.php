<?php get_header(); ?>

<main class="gl" role="main">
	<div class="filter-block">
		<div class="container">
			<strong class="filter-block-title">Найдите нужную плитку</strong>
			<p class="filter-block-desc">Выберите то, что необходимо. Воспользуйтесь удобными фильтрами для точного выбора продукции. Все варианты керамической плитки представлены в <a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>">каталоге</a>. В нашем каталоге более 400 видов керамической плитки от разных производителей.</p>
			<div class="filter-block-selects">
				<?php
				    $args = array(
				    	'taxonomy'     => 'product_cat',
					    'orderby'      => 'name',
					    'show_count'   => 0,
					    'pad_counts'   => 0,
					    'hierarchical' => 0,
					    'title_li'     => '$title',
					    'hide_empty'   => 0,
					    'parent' => 0
					);

					$filter_categories = get_categories( $args );

					$filter_brands = get_terms( 'pa_brand' );
					$filter_sizes = get_terms( 'pa_size' );
					$filter_brands = get_terms( 'pa_brand' );
					$filter_purposes = get_terms( 'pa_purpose' );
					$filter_countries = get_terms( 'pa_country' );
					$filter_materials = get_terms( 'pa_material' );
					$filter_views = get_terms( 'pa_view' );
				?>
				<select name="filter_brand">
					<option value="0">Выберите фабрику</option>
					<?php foreach ($filter_brands as $brand) : ?>
					<option value="<?php echo $brand->term_id; ?>"><?php echo $brand->name; ?></option>
				<?php endforeach; ?>
				</select>
				<select name="category">
					<option value="0">Выберите коллекцию</option>
					<?php foreach ($filter_categories as $category) : ?>
					<option value="<?php echo $category->slug; ?>"><?php echo $category->name; ?></option>
					<?php endforeach; ?>
				</select>
				<select name="filter_size">
					<option value="0">Размер</option>
					<?php foreach ($filter_sizes as $size) : ?>
					<option value="<?php echo $size->term_id; ?>"><?php echo $size->name; ?></option>
				<?php endforeach; ?>
				</select>
				<select name="filter_purpose">
					<option value="0">Назначение</option>
					<?php foreach ($filter_purposes as $purpose) : ?>
					<option value="<?php echo $purpose->term_id; ?>"><?php echo $purpose->name; ?></option>
				<?php endforeach; ?>
				</select>
				<select name="filter_country">
					<option value="0">Страна</option>
					<?php foreach ($filter_countries as $country) : ?>
					<option value="<?php echo $country->term_id; ?>"><?php echo $country->name; ?></option>
				<?php endforeach; ?>
				</select>
				<select name="filter_material">
					<option value="0">Фактура</option>
					<?php foreach ($filter_materials as $material) : ?>
					<option value="<?php echo $material->term_id; ?>"><?php echo $material->name; ?></option>
				<?php endforeach; ?>
				</select>
				<select name="filter_type">
					<option value="0">Применение</option>
					<?php foreach ($filter_views as $view) : ?>
					<option value="<?php echo $view->term_id; ?>"><?php echo $view->name; ?></option>
				<?php endforeach; ?>
				</select>
			</div>
			<form action="/catalog/" method="GET" class="filter-block-form">
				<input type="hidden" name="filtering" value="1">
				<div class="filter-block-bar">
					<input type="submit" value="Подобрать">
				</div>
			</form>
		</div>
	</div>
	<div class="top-index-block">
		<div class="container">
			<?php the_widget('WC_Widget_Product_Categories', array(
				'title' => 'Каталог продукции'
			), array(
				'before_widget' => '<nav class="catalog-menu to-left">',
				'after_widget' => '</nav>',
				'before_title' => '<h4>',
				'after_title' => '</h4>'
			));
			?>
			<div class="action-block to-right">
				<div class="actions-list">
					<a href="#" class="big-action"><img src="<?php bloginfo('template_url'); ?>/img/slide-tmp.png" alt=""></a>
					<div class="two-actions">
						<a href="#" class="small-action"><img src="<?php bloginfo('template_url'); ?>/img/slide-tmp.png" alt=""></a>
						<a href="#" class="small-action"><img src="<?php bloginfo('template_url'); ?>/img/slide-tmp.png" alt=""></a>
					</div>
				</div>
				<a href="#" class="actions-reload">Больше выгодных предложений</a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="goods-block grey-bg">
		<div class="container">
			<?php
				// Sale goods
				$query = array(
				    'posts_per_page' => 8,
				    'no_found_rows' => 1,
				    'orderby' => 'date',
				    'order' => 'DESC',
				    'post_status' => 'publish',
				    'post_type' => 'product',
				    'meta_query' => WC()->query->get_meta_query(),
				    'post__in' => array_merge(array(0), wc_get_product_ids_on_sale())
				);
				$sale_products = new WP_Query($query);
			?>
			<?php if ($sale_products->have_posts()) : ?>
				<div class="goods-part-block">
					<h2>Распродажа</h2>
					<section class="goods-list">
						<?php while ($sale_products->have_posts()) : $sale_products->the_post(); ?>
							<?php
								$the_price = $product->get_price_html();
								$the_sku = $product->get_sku();
								$in_stock = $product->is_in_stock();
								if ($in_stock == true) {
									$stock_text = 'Товар в наличии';
									$stock_class = 'goods-single-exist';
								} else {
									$stock_text = 'Товар отсутствует';
									$stock_class = 'goods-single-not-exist';
								}
								$the_category = $product->get_categories(', ');
								$the_category = end(explode(', ', $the_category));
							?>
							<article class="goods-single">
								<a href="<?php the_permalink(); ?>" class="goods-single-img">
									<?php echo woocommerce_get_product_thumbnail(); ?>
								</a>
								<div class="goods-single-content">
									<span class="goods-single-cat-link"><?php echo $the_category; ?></span>
									<h3 class="goods-single-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<span class="goods-single-marking">Код <?php echo $the_sku; ?></span>
									<span class="goods-single-availability <?php echo $stock_class; ?>"><?php echo $stock_text; ?></span>
									<div class="goods-single-bar">
										<span class="goods-single-price to-left"><?php echo $the_price; ?></span>
										<?php woocommerce_template_loop_add_to_cart(); ?>
										<div class="clearfix"></div>
									</div>
								</div>
							</article>
						<?php endwhile; ?>
						<div class="clearfix"></div>
						<div class="goods-more">
							<div class="goods-more-line"></div>
							<div class="btn-more-wrapper">
								<a href="<?php bloginfo('home'); ?>/sale/" class="btn btn-blue">Посмотреть все</a>
							</div>
						</div>
					</section>
				</div>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
			<?php
				// Recent goods
				$query = array(
				    'posts_per_page' => 8,
				    'no_found_rows' => 1,
				    'post_status' => 'publish',
				    'post_type' => 'product',
				    'orderby' => 'date',
				    'order' => 'DESC',
				    'meta_query' => WC()->query->get_meta_query()
				);
				$recent_products = new WP_Query($query);
			?>
			<?php if ($recent_products->have_posts()) : ?>
				<div class="goods-part-block">
					<h2>Новинки</h2>
					<section class="goods-list">
						<?php while ($recent_products->have_posts()) : $recent_products->the_post(); ?>
							<?php
								$the_price = $product->get_price_html();;
								$the_sku = $product->get_sku();
								$in_stock = $product->is_in_stock();
								if ($in_stock == true) {
									$stock_text = 'Товар в наличии';
									$stock_class = 'goods-single-exist';
								} else {
									$stock_text = 'Товар отсутствует';
									$stock_class = 'goods-single-not-exist';
								}
								$the_category = $product->get_categories(', ');
								$the_category = end(explode(', ', $the_category));
							?>
							<article class="goods-single">
								<a href="<?php the_permalink(); ?>" class="goods-single-img">
									<?php echo woocommerce_get_product_thumbnail(); ?>
								</a>
								<div class="goods-single-content">
									<span class="goods-single-cat-link"><?php echo $the_category; ?></span>
									<h3 class="goods-single-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<span class="goods-single-marking">Код <?php echo $the_sku; ?></span>
									<span class="goods-single-availability <?php echo $stock_class; ?>"><?php echo $stock_text; ?></span>
									<div class="goods-single-bar">
										<span class="goods-single-price to-left"><?php echo $the_price; ?></span>
										<?php woocommerce_template_loop_add_to_cart(); ?>
										<div class="clearfix"></div>
									</div>
								</div>
							</article>
						<?php endwhile; ?>
						<div class="clearfix"></div>
						<div class="goods-more">
							<div class="goods-more-line"></div>
							<div class="btn-more-wrapper">
								<a href="<?php echo get_permalink(woocommerce_get_page_id('shop')); ?>" class="btn btn-blue">Посмотреть все</a>
							</div>
						</div>
					</section>
				</div>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
		</div>
	</div>
	<div class="distribute distribute-bottom grey-bg">
		<div class="container">
			<h3>Оффициальный дистрибьютор фабрик</h3>
			<div class="factories-list">
				<a href="#"><img src="<?php bloginfo('template_url'); ?>/img/fabric/fap.png" alt=""></a>
				<a href="#"><img src="<?php bloginfo('template_url'); ?>/img/fabric/grespania.png" alt=""></a>
				<a href="#"><img src="<?php bloginfo('template_url'); ?>/img/fabric/iris.png" alt=""></a>
				<a href="#"><img src="<?php bloginfo('template_url'); ?>/img/fabric/vallelunga.png" alt=""></a>
				<a href="#"><img src="<?php bloginfo('template_url'); ?>/img/fabric/lea.png" alt=""></a>
				<a href="#"><img src="<?php bloginfo('template_url'); ?>/img/fabric/santagostino.png" alt=""></a>
				<a href="#"><img src="<?php bloginfo('template_url'); ?>/img/fabric/tagina.png" alt=""></a>
				<a href="#"><img src="<?php bloginfo('template_url'); ?>/img/fabric/gardenia.png" alt=""></a>
			</div>
		</div>
	</div>
	<?php $page = get_posts(array('name' => 'index', 'post_type' => 'page')); ?>
	<?php if ($page) : ?>
		<section class="page-bottom-text grey-bg">
			<div class="container">
				<?php echo $page[0]->post_content; ?>
			</div>
		</section>
	<?php endif; ?>
</main>

<?php get_footer(); ?>
