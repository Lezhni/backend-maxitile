<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>

	<div class="content to-right">

		<div class="content-page catalog-page">

      <?php
				global $wp_query;
				$category_name = $wp_query->query_vars['product_cat'];
			?>
			<?php if ( $category_name ) : ?>

				<?php
					$category_data = get_term_by( 'name', $category_name, 'product_cat' );
					$category_id = $category_data->term_id;
				?>
				<?php
					// Main image
					$category_thumbnail = get_woocommerce_term_meta( $category_id, 'thumbnail_id', true );
	  			$image = wp_get_attachment_url( $category_thumbnail );
				?>
				<?php
					// Category gallery
					for ($i = 1; $i <= 6; $i++) {
						$cat_gallery[] = Categories_Multiple_Images::get_image( $category_id, $i );
					}
					$cat_gallery = array_filter( $cat_gallery );
				?>

				<?php
					$loop = new WP_Query( array(
						'post_type' => 'product',
						'posts_per_page' => 1,
						'order_by' => 'date',
						'product_cat' => $category_data->slug,
						'meta_query' => WC()->query->get_meta_query()
					) );
					if ( $loop->have_posts() ) {
						$cat_product = $loop->the_post();
						$product = wc_get_product( get_the_ID() );
						$category_brand = $product->get_attribute( 'brand' );
						$category_country = $product->get_attribute( 'country' );
						$category_purpose = $product->get_attribute( 'purpose' );
            $brand_slug = get_the_terms( $product->id, 'pa_brand' );
            $brand_slug = $brand_slug[0]->slug;
					}
					wp_reset_query();
				?>
      <?php endif; ?>

      <?php if ( is_product_category() ) : ?>
			  <h2 class="collection-distributor">Официальный дистрибьютор <?php echo $category_brand; ?></h1>
      <?php else : ?>
        <h1 class="page-title"><?php woocommerce_page_title(); ?></h1>
      <?php endif; ?>

      <?php if ( is_product_category() ) : ?>

      <div class="images collection-images">
        <a href="<?php echo $image; ?>" itemprop="image" class="woocommerce-main-image zoom" data-rel="prettyPhoto[collection-gallery]">
          <img src="<?php echo $image; ?>" class="attachment-shop_single size-shop_single wp-post-image">
        </a>
        <?php if ( $cat_gallery ) : ?>

          <?php $loop = 0; $columns 	= apply_filters( 'woocommerce_product_thumbnails_columns', 3 ); ?>
          <div class="thumbnails <?php echo 'columns-' . $columns; ?>">
          <?php foreach ( $cat_gallery as $cat_gallery_single ) : ?>

        			<?php
                $classes = array( 'zoom' );
        			  if ( $loop == 0 || $loop % $columns == 0 )
        				  $classes[] = 'first';
        			  if ( ( $loop + 1 ) % $columns == 0 )
        				  $classes[] = 'last';
        			  $image_link = $cat_gallery_single;
        			  if ( ! $image_link )
        				  continue;
        			  $image_class = esc_attr( implode( ' ', $classes ) );
              ?>
              <a href="<?php echo $image_link; ?>" class="<?php echo $image_class; ?>" data-rel="prettyPhoto[collection-gallery]"><img width="180" height="180" src="<?php echo $image_link; ?>" class="attachment-shop_thumbnail size-shop_thumbnail"></a>
        			<?php $loop++; ?>

          <?php endforeach; ?>
          </div>

          <?php endif; ?>
      </div>
      <div class="summary entry-summary collection-info">

        <h1 class="collection-title to-left"><span>Коллекция</span><?php woocommerce_page_title(); ?></h1>
        <a href="<?php echo bloginfo( 'url' ) . '/brands/' . $brand_slug; ?>" class="to-factory btn to-right">Другие коллекции фабрики</a>
        <div class="clearfix"></div>

        <div class="product-attr">
    			<p class="product-attr-title">Характеристики</p>
    			<?php if ( $category_brand ) : ?>
    				<dl class="attr-brand">
    					<dt>Фабрика:</dt>
    					<dd><?php echo $category_brand; ?></dd>
    				</dl>
    			<?php endif; ?>
    			<?php if ( $category_purpose ) : ?>
    				<dl class="attr-purpose">
    					<dt>Назначение:</dt>
    					<dd><?php echo $category_purpose; ?></dd>
    				</dl>
    			<?php endif; ?>
    			<?php if ( $category_country ) : ?>
    				<dl class="attr-country">
    					<dt>Страна производителя:</dt>
    					<dd><?php echo $category_country; ?></dd>
    				</dl>
    			<?php endif; ?>
    		</div>
        <?php endif; ?>

      <?php if ( is_product_category() ) : ?>
      </div>
      <div class="clearfix"></div>

			<section class="collection-advantages">
			  <strong>5 причин купить у нас:</strong>
        <div class="collection-advantages-list">
          <figure>
            <div class="img-wrapper"><img src="<?php bloginfo( 'template_url' ); ?>/img/collection-advantage-stock.png" alt=""></div>
            <figcaption>Самый большой склад</figcaption>
          </figure>
          <figure>
            <div class="img-wrapper"><img src="<?php bloginfo( 'template_url' ); ?>/img/collection-advantage-price.png" alt=""></div>
            <figcaption>Выгодная цена</figcaption>
          </figure>
          <figure>
            <div class="img-wrapper"><img src="<?php bloginfo( 'template_url' ); ?>/img/collection-advantage-terms.png" alt=""></div>
            <figcaption>Минимальные сроки</figcaption>
          </figure>
          <figure>
            <div class="img-wrapper"><img src="<?php bloginfo( 'template_url' ); ?>/img/collection-advantage-3d.png" alt=""></div>
            <figcaption>Бесплатная 3D визуализация</figcaption>
          </figure>
          <figure>
            <div class="img-wrapper"><img src="<?php bloginfo( 'template_url' ); ?>/img/collection-advantage-showroom.png" alt=""></div>
            <figcaption>Крупнейший шоурум</figcaption>
          </figure>
        </div>
			</section>

      <?php endif; ?>
		</div>

		<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>

    <?php
      /**
       * woocommerce_archive_description hook
       *
       * @hooked woocommerce_taxonomy_archive_description - 10
       * @hooked woocommerce_product_archive_description - 10
       */
      do_action( 'woocommerce_archive_description' );
    ?>

	</div>
	<div class="clearfix"></div>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>


<?php get_footer( 'shop' ); ?>
