<?php
/**
 * Loop Add to Cart
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$button_text = 'В корзину';
$button_added_class = '';
foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
	$_product = $values['data'];

	if( get_the_ID() == $_product->id ) {
		$button_text = 'В корзине';
		$button_added_class = 'just-added';
		break;
	}
}

if ($product->is_in_stock()) {

	echo apply_filters( 'woocommerce_loop_add_to_cart_link',
		sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="goods-single-buy btn to-right button %s product_type_%s">%s</a>',
			esc_url( $product->add_to_cart_url() ),
			esc_attr( $product->id ),
			esc_attr( $product->get_sku() ),
			esc_attr( isset( $quantity ) ? $quantity : 1 ),
			$product->is_purchasable() && $product->is_in_stock() ? "add_to_cart_button $button_added_class" : '',
			esc_attr( $product->product_type ),
			esc_html( $button_text )
		),
	$product );
} else {
	echo apply_filters( 'woocommerce_loop_add_to_cart_link',
		sprintf( '<a href="#request" rel="nofollow" data-product_id="%s" data-name="%s" class="goods-single-buy leave-request btn-grey btn to-right button">%s</a>',
			esc_attr( $product->id ),
			esc_attr( $product->get_title() ),
			esc_html( 'Заявка' )
		),
	$product );
}
