<?php
/**
 * Displayed when no products are found matching the current query.
 *
 * Override this template by copying it to yourtheme/woocommerce/loop/no-products-found.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<div class="catalog-goods-list goods-list">
	<p class="woocommerce-info" style="margin: 0; padding: 0 25px 25px;"><?php _e( 'No products were found matching your selection.', 'woocommerce' ); ?></p>
</div>