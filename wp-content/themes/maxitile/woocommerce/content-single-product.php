<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php global $product; ?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<h1 class="page-title goods-title"><?php the_title(); ?></h1>

	<?php
		/**
		 * woocommerce_before_single_product_summary hook
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary product-info">

		<div class="product-price-block">
			<span class="product-price"><?php echo $product->get_price_html(); ?></span>

			<?php woocommerce_quantity_input( array(
		      'min_value' => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
		      'max_value' => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product )
		    ) );
		    ?>

			<span class="product-price-units"><?php echo $product->get_attribute('units'); ?></span>

			<?php woocommerce_template_loop_add_to_cart(); ?>

			<meta itemprop="price" content="<?php echo $product->get_price(); ?>" />
			<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
			<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
		</div>

		<div class="product-attr">
			<p class="product-attr-title">Характеристики</p>
			<?php if ($product->get_attribute('brand')) : ?>
				<dl class="attr-brand">
					<dt>Фабрика:</dt>
					<dd><?php echo $product->get_attribute('brand'); ?></dd>
				</dl>
			<?php endif; ?>
			<?php if ($product->get_attribute('purpose')) : ?>
				<dl class="attr-purpose">
					<dt>Назначение:</dt>
					<dd><?php echo $product->get_attribute('purpose'); ?></dd>
				</dl>
			<?php endif; ?>
			<?php if ($product->get_attribute('country')) : ?>
				<dl class="attr-country">
					<dt>Страна производителя:</dt>
					<dd><?php echo $product->get_attribute('country'); ?></dd>
				</dl>
			<?php endif; ?>
		</div>
		<div class="product-attr product-tech-attr">
			<p class="product-attr-title">Технические характеристики</p>
			<?php if ($product->get_attribute('size')) : ?>
				<dl class="attr-size">
					<dt>Размер:</dt>
					<dd><?php echo $product->get_attribute('size'); ?></dd>
				</dl>
			<?php endif; ?>
			<?php if ($product->get_attribute('min-order')) : ?>
				<dl class="attr-min-order">
					<dt>Мин. заказ:</dt>
					<dd><?php echo $product->get_attribute('min-order'); ?></dd>
				</dl>
			<?php endif; ?>
		</div>
		<?php if (get_the_content()) : ?>

			<article class="product-desc">
				<?php the_content(); ?>
			</article>

		<?php endif; ?>
	</div><!-- .summary -->
	<div class="clearfix"></div>


	<section class="collection-advantages">
		<strong>5 причин купить у нас:</strong>
		<div class="collection-advantages-list">
			<figure>
				<div class="img-wrapper"><img src="<?php bloginfo( 'template_url' ); ?>/img/collection-advantage-stock.png" alt=""></div>
				<figcaption>Самый большой склад</figcaption>
			</figure>
			<figure>
				<div class="img-wrapper"><img src="<?php bloginfo( 'template_url' ); ?>/img/collection-advantage-price.png" alt=""></div>
				<figcaption>Выгодная цена</figcaption>
			</figure>
			<figure>
				<div class="img-wrapper"><img src="<?php bloginfo( 'template_url' ); ?>/img/collection-advantage-terms.png" alt=""></div>
				<figcaption>Минимальные сроки</figcaption>
			</figure>
			<figure>
				<div class="img-wrapper"><img src="<?php bloginfo( 'template_url' ); ?>/img/collection-advantage-3d.png" alt=""></div>
				<figcaption>Бесплатная 3D визуализация</figcaption>
			</figure>
			<figure>
				<div class="img-wrapper"><img src="<?php bloginfo( 'template_url' ); ?>/img/collection-advantage-showroom.png" alt=""></div>
				<figcaption>Крупнейший шоурум</figcaption>
			</figure>
		</div>
	</section>

	<?
		// Get current collection
		$collection = get_the_terms( $product->ID, 'product_cat' )[0];

		// Get all goods from collection
		$args = array(
			'post_type' => 'product',
			'product_cat' => $collection->slug,
			'post__not_in' => array(get_the_ID()),
		);
		$collection_goods = new WP_Query($args);
	?>
	<?php if ($collection_goods->have_posts()) : ?>
		<div class="goods-list catalog-goods-list category-goods-list">
			<h2>Плитка коллекции <?php echo $collection->name; ?></h2>
			<?php while ($collection_goods->have_posts()) : $collection_goods->the_post(); ?>
			<?php
				global $product;
				$the_price = $product->get_price_html();
				$the_image = $product->get_image('100%');
				$the_sku = $product->get_sku();
				$in_stock = $product->is_in_stock();
				if ($in_stock == true) {
					$stock_text = 'Товар в наличии';
					$stock_class = 'goods-single-exist';
				} else {
					$stock_text = 'Товар отсутствует';
					$stock_class = 'goods-single-not-exist';
				}
			?>
			<article <?php post_class( 'goods-single goods-hover' ); ?>>
			<div class="hover-el">
				<a href="<?php the_permalink(); ?>" class="goods-single-img">
					<?php echo $the_image; ?>
				</a>
				<div class="goods-single-content">
					<span class="goods-single-cat-link"></span>
					<h3 class="goods-single-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<div class="goods-single-bar">
						<span class="goods-single-price to-left"><?php echo $the_price; ?></span>
						<?php woocommerce_template_loop_add_to_cart(); ?>
						<div class="clearfix"></div>
					</div>
					<div class="product-attr">
						<dl>
							<dt>Размер:</dt>
							<dd><?php echo $product->get_attribute( 'size' ); ?></dd>
							<div class="clearfix"></div>
						</dl>
						<dl>
							<dt>Поверхность:</dt>
							<dd><?php echo $product->get_attribute( 'surface' ); ?></dd>
							<div class="clearfix"></div>
						</dl>
						<dl>
							<dt>Образец в шоу-руме:</dt>
							<dd><?php echo $product->get_attribute( 'showroom' ); ?></dd>
							<div class="clearfix"></div>
						</dl>
					</div>
					<span class="goods-single-marking">Код <?php echo $the_sku; ?></span>
					<span class="goods-single-availability <?php echo $stock_class; ?>"><?php echo $stock_text; ?></span>
				</div>
			</div>
			</article>
			<?php endwhile; ?>
		</div>
		<?php wp_reset_query(); ?>
	<?php endif; ?>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
