<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] ) {
	$classes[] = 'first';
}
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
	$classes[] = 'last';
}
?>
<?php
	$classes[] = 'goods-single';
	$classes[] = 'goods-hover';
	$the_price = $product->get_price_html();
	$the_sku = $product->get_sku();
	$in_stock = $product->is_in_stock();
	if ($in_stock == true) {
		$stock_text = 'Товар в наличии';
		$stock_class = 'goods-single-exist';
	} else {
		$stock_text = 'Товар отсутствует';
		$stock_class = 'goods-single-not-exist';
	}
	$the_category = $product->get_categories(', ');
	$the_category = end(explode(', ', $the_category));
?>

<article <?php post_class($classes); ?>>
<div class="hover-el">
	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

	<a href="<?php the_permalink(); ?>" class="goods-single-img">
		<?php echo woocommerce_get_product_thumbnail('shop_catalog'); ?>
	</a>
	<div class="goods-single-content">
		<span class="goods-single-cat-link"><?php echo $the_category; ?></span>
		<h3 class="goods-single-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<div class="goods-single-bar">
			<span class="goods-single-price to-left"><?php echo $the_price; ?></span>
			<?php woocommerce_template_loop_add_to_cart(); ?>
			<div class="clearfix"></div>
		</div>
		<div class="product-attr">
			<dl>
				<dt>Размер:</dt>
				<dd><?php echo $product->get_attribute( 'size' ); ?></dd>
        <div class="clearfix"></div>
			</dl>
			<dl>
				<dt>Поверхность:</dt>
				<dd><?php echo $product->get_attribute( 'surface' ); ?></dd>
        <div class="clearfix"></div>
			</dl>
			<dl>
				<dt>Образец в шоу-руме:</dt>
				<dd><?php echo $product->get_attribute( 'showroom' ); ?></dd>
				<div class="clearfix"></div>
			</dl>
		</div>
		<span class="goods-single-marking">Код <?php echo $the_sku; ?></span>
		<span class="goods-single-availability <?php echo $stock_class; ?>"><?php echo $stock_text; ?></span>
	</div>
</div>
</article>
