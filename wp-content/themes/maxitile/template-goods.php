<?php
/**
 * Template Name: Страница с товарами
 */
?>

<?php get_header(); ?>

<?php if (have_posts()) : ?>
	<main class="gl grey-bg content-wrapper" role="main">
		<div class="container">
			<nav class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
			    <?php if(function_exists('bcn_display')) : ?>
			        <?php bcn_display(); ?>
			    <?php endif; ?>
			</nav>

			<?php get_sidebar(); ?>

			<section class="content to-right">
				<div class="content-page catalog-page">
					<?php while (have_posts()) : the_post(); ?>
						<h1 class="page-title"><?php the_title(); ?></h1>
						<?php the_content(); ?>
					<?php endwhile; ?>
				</div>
				<?php
					$shortcode = get_field('goods_shortcode');
					echo do_shortcode($shortcode);
				?>
			</section>
			<div class="clearfix"></div>
		</div>
	</main>
<?php endif; ?>

<?php get_footer(); ?>
