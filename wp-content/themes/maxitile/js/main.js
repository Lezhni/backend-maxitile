$ = jQuery;

$(document).on('ready', init);
$(document).on('click', '.leave-request', showRequestForm);
$(document).on('change', 'input[name="quantity"]', changeQuantity);
$(document).on('change', '.filter-block-selects select', buildFilter);
$(document).on('click', '.add_to_cart_button', showAddToCartNotice);


function init() {

	var f_h = 0;
	$('footer.gl .container > *:not(.clearfix)').each(function() {
		if ($(this).height() > f_h) {
			f_h = $(this).height();
		}
	});
	$('footer.gl .container > *:not(.clearfix)').css('height', f_h + 7);

	if ($('.woocommerce-main-image img').width() > $('.woocommerce-main-image img').height()) {
		$('.woocommerce-main-image').addClass('landscape');
	} else if ($('.woocommerce-main-image img').width() === $('.woocommerce-main-image img').height()) {
		$('.woocommerce-main-image').addClass('equal');
	} else {
		$('.woocommerce-main-image').addClass('portrait');
	}

	$('.goods-single').each(function() {
		var img = $(this).find('img');
		if (img.width() > img.height()) {
		img.addClass('landscape');
		} else if (img.width() === img.height()) {
			img.addClass('equal');
		} else {
			img.addClass('portrait');
		}
	});
}

function showRequestForm() {

	var goods_name = $(this).attr('data-name');
	$('#request .goods-name').text(goods_name);
	$('#request [name=request-goods]').val(goods_name);

	$('#request [name=request-name]').val($('#request input.request-name').val());
	$('#request [name=request-email]').val($('#request input.request-email').val());
	$('#request [name=request-phone]').val($('#request input.request-phone').val());

	$.magnificPopup.open({
	  items: {
	    src: '#request',
	    type: 'inline'
	  }
	});

	return false;
}

function changeQuantity() {

	var quantity = $(this).val();
	$(this).closest('.product-price-block').find('.to-cart').attr('data-quantity', quantity);
}

function showAddToCartNotice() {

	var btn = $(this);
	if (btn.hasClass('added') && !btn.hasClass('loading')) {
		var productName = btn.closest('.goods-single').find('.goods-single-title').text();
		var productLink = btn.closest('.goods-single').find('.goods-single-title a').attr('href');
		var noticeText = 'Товар <a href="' + productLink + '">' + productName + '</a> добавлен в корзину. <a href="/checkout">Оформить покупку</a>';
		$('.add-to-cart-notice p').html(noticeText);
		$('.add-to-cart-notice').slideDown();
		btn.text('В корзине');
		setTimeout(function() {
			$('.add-to-cart-notice').slideUp();
		}, 5000);
	} else {
		var checkAdding = setInterval(function() {
			if (btn.hasClass('added') && !btn.hasClass('loading')) {
				var productName = btn.closest('.goods-single').find('.goods-single-title').text();
				var productLink = btn.closest('.goods-single').find('.goods-single-title a').attr('href');
				var noticeText = 'Товар <a href="' + productLink + '">' + productName + '</a> добавлен в корзину. <a href="/checkout">Оформить покупку</a>';
				$('.add-to-cart-notice p').html(noticeText);
				$('.add-to-cart-notice').slideDown();
				btn.text('В корзине');
				setTimeout(function() {
					$('.add-to-cart-notice').slideUp();
				}, 5000);

				clearInterval(checkAdding);
			}
		}, 300);
	}
}

function buildFilter() {

	var val = $(this).find('option:selected').val();
	var attr = $(this).attr('name');
	if (attr === 'category') {
		if (parseInt(val) === 0) {
			$('.filter-block-form').attr('action', '/catalog/');
		} else {
			$('.filter-block-form').attr('action', '/category/' + val + '/');
		}

		return false;
	}
	if (parseInt(val) === 0) {
		$('.filter-block-form').find('input[name=' + attr + ']').remove();
	} else {
		if ($('.filter-block-form').find('input[name=' + attr + ']').length > 0) {
			$('.filter-block-form').find('input[name=' + attr + ']').val(val);
		} else {
			$('.filter-block-form').append('<input type="hidden" name="' + attr + '" value="' + val + '">');
		}
	}
}
